import React from "react";

import ColourChangeComponent from "./ColourChangeComponent";

const App = () => (
  <main>
    <ColourChangeComponent />
  </main>
);

export default App;
