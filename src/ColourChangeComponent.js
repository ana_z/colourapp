import React, { useState } from "react";
import styled from "@emotion/styled";
import { SketchPicker } from "react-color";

const ContentWrapper = styled("section")`
  height: 100vh;
  width: 100%;
  padding: 50px 100px;
  display: grid;
  grid-template-columns: 80% 20%;
  justify-content: space-between;
  align-items: center;
  background: ${({ selectedColor }) => selectedColor};
  box-sizing: border-box;
`;

const Button = styled.button`
  width: max-content;
  padding: 15px;
  border-radius: 5px;
  font-size: 1em;
  cursor: pointer;
  background: transparent;
  color: #fff;
  border: 2px solid #fff;
  text-transform: uppercase;

  &:hover {
    color: #3f3f3f;
    background: #fff;
    border-color: transparent;
    transition: all ease-out 0.3s;
  }
`;

const RandomeColourLink = styled.p`
  text-align: center;
  cursor: pointer;
  font-size: 1.2em;
`;

const ColorPickerWrapper = styled.div`
  display: grid;
  justify-content: start;
`;

const ColourChangeComponent = () => {
  const [selectedColor, setSelectedColor] = useState("#abe5c8");
  const [colorPickerOpen, setColorPickerOpen] = useState(false);
  return (
    <>
      <ContentWrapper selectedColor={selectedColor}>
        <ColorPickerWrapper>
          {colorPickerOpen && (
            <>
              <SketchPicker
                width="400px"
                color={selectedColor}
                onChange={(newColor) => setSelectedColor(newColor.hex)}
                disableAlpha
              />
              <RandomeColourLink
                onClick={() =>
                  setSelectedColor(
                    `#${Math.floor(Math.random() * 16777215).toString(16)}`
                  )
                }
              >
                Pick a random colour
              </RandomeColourLink>
            </>
          )}
        </ColorPickerWrapper>
        <Button onClick={() => setColorPickerOpen(!colorPickerOpen)}>
          {colorPickerOpen ? "Looks good" : "Change Colour"}
        </Button>
      </ContentWrapper>
    </>
  );
};

export default ColourChangeComponent;
